# Protect photo Viewer

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## INTRODUCTION
Protect photo is a module that prevents image theft by preventing 
right-click and obscuring the image source in the console log.
it helps prevent image theft by disrupting traditional 
user interactions to download/copy images.

This is useful for copyrighted images.

This module uses the HTML5 CanvasAPI and the Protected
Image theme to protect your images. 
It disables and prevents the client from downloading, copying,
and duplicating image titles by transferring the image to a canvas.
Unlike images that are protected by layers, 
this prevents the source of the image 
from being visible through the developer tools.
## REQUIREMENTS

This module dependency on jquery_ui <https://www.drupal.org/project/jquery_ui>


## INSTALLATION

- Download and Enable the module similar to other drupal module.
- Visit any Image fields in Display settings, 
- you will be able to find the "Protect photo Viewer"
- Select this one and you would be able to select image styles and other config.
- Example: admin/structure/types/manage/page/display

## CONFIGURATION

- Visit any image fields display settings, you will be able to find
the Protect photo Viewer formatter, select this one and one can also
select image styles.
Ex: admin/structure/types/manage/<content-type-machine-name>/display
- Have the image field settings "Allowed number of values"
to unlimited / limited(more than 1 image).
- Have a custom image style defined under
<http://d10.local/admin/config/media/image-styles>
- Add content & upload more than 1 images to the node and Save..
On node view, Image Compare Viewer will appear for that image field.


## MAINTAINERS

 - Ghazali Mustapha (g.mustapha) - <https://www.drupal.org/u/gmustapha>
